package models

// Config model
type Config struct {
	LogLevel string
	HTTPPort string

	PostgresHost     string
	PostgresPort     int
	PostgresDatabase string
	PostgresUser     string
	PostgresPassword string
}
