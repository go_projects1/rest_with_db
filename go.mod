module mygo_projects/rest_with_db

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jmoiron/sqlx v1.2.0
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.0.0
	github.com/spf13/cast v1.3.1
)
