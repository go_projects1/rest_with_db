package config

import (
	"log"
	"mygo_projects/rest_with_db/models"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

func Load() models.Config {

	if err := godotenv.Load(); err != nil {
		log.Print("No .env file found")
	}

	config := models.Config{}

	config.LogLevel = cast.ToString(getOrReturnDefaultValue("LOG_LEVEL", "debug"))
	config.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":7077"))

	config.PostgresHost = cast.ToString(getOrReturnDefaultValue("POSTGRES_HOST", "localhost"))
	config.PostgresPort = cast.ToInt(getOrReturnDefaultValue("POSTGRES_PORT", 5432))
	config.PostgresDatabase = cast.ToString(getOrReturnDefaultValue("POSTGRES_DATABASE", "mypgdb"))
	config.PostgresUser = cast.ToString(getOrReturnDefaultValue("POSTGRES_USER", "mypg_user"))
	config.PostgresPassword = cast.ToString(getOrReturnDefaultValue("POSTGRES_PASSWORD", "mypg_user"))

	return config
}
func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	_, exists := os.LookupEnv(key)

	if exists {
		return os.Getenv(key)
	}
	return defaultValue
}
