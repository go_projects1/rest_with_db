package main

import (
	"fmt"
	"mygo_projects/rest_with_db/config"
	"mygo_projects/rest_with_db/models"
	"net/http"

	"github.com/gin-gonic/gin"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func main() {
	cfg := config.Load()

	router := gin.Default()

	router.GET("/ping", getPing)
	router.GET("/getUsers", getUsers)

	router.Run(cfg.HTTPPort)
	//comment
}

func getUsers(c *gin.Context) {
	c.JSON(http.StatusOK, getUsersModel())
}

func getUsersModel() []models.Client {
	cfg := config.Load()
	psqlConnString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresDatabase)
	fmt.Println(psqlConnString)
	db, err := sqlx.Connect("postgres", psqlConnString)

	if err != nil {
		fmt.Println(err.Error())
		//log.Error("postgres connect error", logger.Error(err))
		return []models.Client{}
	}
	db.Begin()

	q := `Select id,name From Clients`
	var clients []models.Client
	rows, err := db.NamedQuery(q, nil)
	if err != nil {
		fmt.Println(err.Error())
	} else {
		for rows.Next() {
			var client models.Client
			err2 := rows.Scan(&client.ID, &client.NAME)
			if err2 != nil {
				fmt.Println(err.Error())
			}
			clients = append(clients, client)
		}
	}
	return clients
}

func getPing(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "welcome",
	})
}
